﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.DAL.Entities;

namespace Weather.DAL.EF
{
    public class DatabaseContext : DbContext, IDbContextFactory<DatabaseContext>
    {
        public DatabaseContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            
        }

        protected DatabaseContext()
        {
            
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<CityObservations> CityObservations { get; set; }
        public DatabaseContext Create()
        {
            throw new NotImplementedException();
        }
    }
}
