﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.DAL.Entities
{
    public class Observations
    {
        public int Id { get; set; }
        public string CityConditionsIconUrl { get; set; }
        public DateTime ObservationTime { get; set; }
        public float PressureMb { get; set; }
        public float RelativeHumidity { get; set; }
        public float TempC { get; set; }
        public string WeatherDescription { get; set; }
        public string WindDir { get; set; }
        public float WindKph { get; set; }
        public float WindGustKph { get; set; }
    }
}
