﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.DAL.Entities
{
    public class CityObservations
    {
        public int Id { get; set; }
        public virtual City City { get; set; }
        public virtual Observations Observations { get; set; }
        public DateTime ObservationsDate { get; set; }
    }
}
