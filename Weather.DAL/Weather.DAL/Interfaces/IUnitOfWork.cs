﻿using System;

namespace Weather.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICRUDRepository CRUDRepository { get; }
        void Save();
    }
}