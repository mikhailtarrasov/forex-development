﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.DAL.Interfaces
{
    public interface ICRUDRepository
    {
        IQueryable<T> Select<T>() where T : class;
        void Create<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        T Find<T>(int id) where T : class;
    }
}
