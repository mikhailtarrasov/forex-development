﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.DAL.EF;
using Weather.DAL.Interfaces;

namespace Weather.DAL.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;
        private readonly ICRUDRepository _CRUDRepository;

        public UnitOfWork()
        {
            _context = new DatabaseContext(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\Mikhail\Source\Repos\forex-development\DB\WeatherDB.mdf;Integrated Security=True;Connect Timeout=30");
            _CRUDRepository = new CRUDRepository(_context);
        }

        public ICRUDRepository CRUDRepository
        {
            get { return this._CRUDRepository; }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
