﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Weather.DAL.EF;
using Weather.DAL.Interfaces;

namespace Weather.DAL.Repository
{
    public class CRUDRepository : ICRUDRepository
    {
        private readonly DatabaseContext _db;

        public CRUDRepository(DatabaseContext context)
        {
            _db = context;
        }

        public IQueryable<T> Select<T>() where T : class
        {
            return _db.Set<T>();
        }

        public void Create<T>(T entity) where T : class
        {
            _db.Entry(entity).State = EntityState.Added;
        }

        public void Update<T>(T entity) where T : class
        {
            _db.Entry(entity).State = EntityState.Modified;
        }

        public void Delete<T>(T entity) where T : class
        {
            _db.Entry(entity).State = EntityState.Deleted;
        }

        public T Find<T>(int id) where T : class
        {
            return _db.Set<T>().Find(id);
        }
    }
}
