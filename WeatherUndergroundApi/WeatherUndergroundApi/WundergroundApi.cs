﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Weather.BLL.DTO;
using Weather.BLL.Interfaces;

namespace WeatherUndergroundApi
{
    public class WundergroundApi : WundergroundPrivateConf, IWeatherApi
    {
        private const string ApiUri = "http://api.wunderground.com/api/";
        private const string Lang = "RU";
        private const string Format = ".json";
        private const string QueryStart = "q/";

        private Rootobject SendRequest(List<string> features, string query)
        {
            var webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            var requestString = ApiUri + ApiKey + "/";
            foreach (var param in features)
            {
                requestString += param + "/";
            }
            requestString += "lang:" + Lang + "/";
            requestString += QueryStart + query;
            requestString += Format;

            string json = webClient.DownloadString(requestString);
            var deserializedCityConditions = JsonConvert.DeserializeObject<Rootobject>(json);

            return deserializedCityConditions;
        }

        public ObservationsDTO GetCityConditions(string country, string city)
        {
            var features = new List<string>();
            features.Add("conditions");
            string location = country + "/" + city;
            var rootObject = SendRequest(features, location);
            var observation = rootObject.current_observation;

            var observationDto = new ObservationsDTO()
            {
                TempC = observation.temp_c,
                CityConditionsIconUrl = observation.icon_url,
                PressureMb = Single.Parse(observation.pressure_mb),
                WeatherDescription = observation.weather,
                WindDir = observation.wind_dir,
                WindKph = observation.wind_kph,
                WindGustKph = observation.wind_gust_kph,
                RelativeHumidity = Single.Parse(observation.relative_humidity.Replace("%", "")),
                // TO DO... Распарсить строку и привести к дате
                // "Sun, 30 Jul 2017 23:09:06 +0500"
                //ObservationTime = new DateTime(observation.observation_time_rfc822)     
            };
            return observationDto;
        }
    }
}
