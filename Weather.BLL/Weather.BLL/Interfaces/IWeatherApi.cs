﻿using Weather.BLL.DTO;

namespace Weather.BLL.Interfaces
{
    public interface IWeatherApi
    {
        ObservationsDTO GetCityConditions(string country, string city);
    }
}