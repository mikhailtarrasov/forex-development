﻿using System.Collections.Generic;
using Weather.BLL.DTO;
using Weather.DAL.Interfaces;

namespace Weather.BLL.Interfaces
{
    public interface IWeatherService
    {
        void UpdateCityObservations(CityDTO city);
        List<CityObservationsDTO> GetObservationDataForCityOverLastNDays(CityDTO city, int nDays);
    }
}