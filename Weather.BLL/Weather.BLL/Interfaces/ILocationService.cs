﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Weather.BLL.DTO;

namespace Weather.BLL.Interfaces
{
    public interface ILocationService
    {
        List<CountryDTO> GetCountries();
    }
}
