﻿using System;

namespace Weather.BLL.DTO
{
    public class ObservationsDTO
    {
        public string CityConditionsIconUrl  { get; set; }
        public DateTime ObservationTime { get; set; }
        public float PressureMb { get; set; }
        public float RelativeHumidity { get; set; }
        public float TempC { get; set; }
        public string WeatherDescription { get; set; }
        public string WindDir { get; set; }
        public float WindKph { get; set; }
        public float WindGustKph { get; set; }
    }
}