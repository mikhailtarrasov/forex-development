﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.BLL.DTO
{
    public class CityObservationsDTO
    {
        public virtual CityDTO City { get; set; }
        public virtual ObservationsDTO Observations { get; set; }
        public DateTime ObservationsDate { get; set; }
    }
}
