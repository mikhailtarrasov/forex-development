﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.BLL.DTO
{
    public class CityDTO
    {
        public string Name { get; set; }
        public virtual CountryDTO Country { get; set; }
    }
}
