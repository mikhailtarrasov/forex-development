using System.Collections.Generic;

namespace Weather.BLL.DTO
{
    public class CountryDTO
    {
        public string Name { get; set; }
        public virtual List<CityDTO> Cities { get; set; }
    }
}