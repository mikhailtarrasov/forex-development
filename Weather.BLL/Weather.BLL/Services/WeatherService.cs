﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using Weather.BLL.DTO;
using Weather.BLL.Interfaces;
using Weather.DAL.Entities;
using Weather.DAL.Interfaces;

namespace Weather.BLL.Services
{
    public class WeatherService : IWeatherService
    {
        private IUnitOfWork _db;
        private IWeatherApi _api;
        private IMapper _mapper;

        public WeatherService(IUnitOfWork uow, IWeatherApi api)
        {
            _db = uow;
            _api = api;
            ConfigureMapper();
        }

        private void ConfigureMapper()
        {
            var config =  new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ObservationsDTO, Observations>();
                cfg.CreateMap<CityDTO, City>();
                cfg.CreateMap<CityObservations, CityObservationsDTO>(); // Нужно узнать, приводить ли он вложенные типы города и наблюдений
            });
            _mapper = config.CreateMapper();
        }

        private ObservationsDTO GetCurrentObservationForCity(CityDTO city)
        {
            var countryName = city.Country.Name;
            var cityName = city.Name;
            return _api.GetCityConditions(countryName, cityName);
        }

        private City GetCityFromDatabase(CityDTO cityDTO)
        {
            var city = 
                _db.CRUDRepository.Select<City>()
                .FirstOrDefault(x => x.Name == cityDTO.Name && x.Country.Name == cityDTO.Country.Name);
            return city;
        }

        private void SaveNewObservations(ObservationsDTO observationsDTO, CityDTO cityDTO)
        {
            var observations = _mapper.Map<Observations>(observationsDTO);
            var city = GetCityFromDatabase(cityDTO);
            var cityObservations = new CityObservations()
            {
                City = city,
                Observations = observations,
                ObservationsDate = observations.ObservationTime.Date
            };

            //_db.CRUDRepository.Create(observations);  // Проверить, создается ли запись с наблюдениями
            _db.CRUDRepository.Create(cityObservations);
            _db.Save();
        }

        public void UpdateCityObservations(CityDTO city)
        {
            var cityObservations = GetCurrentObservationForCity(city);
            SaveNewObservations(cityObservations, city);
        }

        public List<CityObservationsDTO> GetObservationDataForCityOverLastNDays(CityDTO city, int nDays)
        {
            var dbCity = GetCityFromDatabase(city);

            var observationsDataForCity = _db.CRUDRepository.Select<CityObservations>().Where(x => 
                x.City == dbCity && 
                x.ObservationsDate > DateTime.UtcNow.Date.AddDays(-10));
            
            var cityObservationsList = new List<CityObservationsDTO>();

            foreach (var cityObservations in observationsDataForCity)
            {
                cityObservationsList.Add(_mapper.Map<CityObservationsDTO>(cityObservations));
            }
            return cityObservationsList;
        }
    }
}