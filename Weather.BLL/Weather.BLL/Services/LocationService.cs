﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Weather.BLL.DTO;
using Weather.BLL.Interfaces;
using Weather.DAL.Entities;
using Weather.DAL.Interfaces;

namespace Weather.BLL.Services
{
    public class LocationService : ILocationService
    {
        private IUnitOfWork _db;
        private IMapper _mapper;

        public LocationService(IUnitOfWork db)
        {
            _db = db;
            ConfigureMapper();
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(cfg => 
                cfg.CreateMap<IQueryable<Country>, List<CountryDTO>>());
            _mapper = config.CreateMapper();
        }

        public List<CountryDTO> GetCountries()
        {
            return _mapper.Map<List<CountryDTO>>(_db.CRUDRepository.Select<Country>());
        }
    }
}
