﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherUndergroundApi;

namespace WeatherUndergroundClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var wuApi = new WundergroundApi();
            var cityConditions = wuApi.GetCityConditions("Russia", "Chelyabinsk");
            Console.WriteLine("Icon URL - " + cityConditions.icon_url);
            Console.WriteLine("Observation time - " + cityConditions.observation_time);
            Console.WriteLine("Температура - " + cityConditions.temp_c);
            Console.WriteLine("Погода - " + cityConditions.weather);
            Console.WriteLine("Направление ветра - " + cityConditions.wind_dir);
            Console.WriteLine("Сила ветра (м/с)- " + cityConditions.wind_kph * 60 * 60 / 1000);
            Console.WriteLine("Порывы ветра (м/с)- " + cityConditions.wind_gust_kph * 60 * 60 / 1000);
            Console.WriteLine("Относительная влажность - " + cityConditions.relative_humidity);
            Console.WriteLine("Атмосферное давление, мм.рт.ст. - " + Single.Parse(cityConditions.pressure_mb) *3/4);
            
            Console.ReadKey();

        }
    }
}
