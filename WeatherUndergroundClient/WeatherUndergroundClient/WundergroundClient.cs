﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather;
using WeatherUndergroundApi;

namespace WeatherUndergroundClient
{
    public class WundergroundClient : IWeatherClient
    {
        private WundergroundApi WuApi { get; set; }

        public WundergroundClient()
        {
            WuApi = new WundergroundApi();
        }

        public Observation GetCurrentObservationForCity(string country, string city)
        {
            var currentObservation = WuApi.GetCityConditions(country, city);
            return ConvertResponseTypeToObservation(currentObservation);
        }

        private Observation ConvertResponseTypeToObservation(Current_Observation response)
        {
            var observation = new Observation()
            {
                CityConditionsIconUrl = response.icon_url,
                //ObservationTime = new DateTime(response.),
                //PressureMb = response.pressure_mb,
                RelativeHumidity = Single.Parse(response.relative_humidity.Replace("%", "")),
                TempC = response.temp_c,
                WeatherDescription = response.weather,
                WindDir = response.wind_dir,
                WindKph = response.wind_kph,
                WindGustKph = response.wind_gust_kph
            };

            return observation;
        }

        //public Observation ConvertResponseTypeToObservation<T>(T response)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
